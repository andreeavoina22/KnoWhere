import React, { Component } from 'react';
import classes from './WebGL.css';
import App3d from '../../3d/App3d';
import Cube from '../../3d/cube';

class WebGL extends Component {
    componentDidMount() {
         this.app3d = new App3d();
         this.app3d.start();
         this.app3d.camera.position.z = 400;
         this.cube = new Cube();
         this.app3d.scene.add(this.cube);
    }
    
    render() {
        return (
            <div id='webgl' className={classes.container}></div>
        );
    }
}

export default WebGL;