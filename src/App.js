import React, { Component } from 'react';
import './App.css';
import WebGL from './components/WebGL/WebGL';

class App extends Component {
  render() {
    return (
      <div className="App">
            <WebGL/>
      </div>
    );
  }
}

export default App;
