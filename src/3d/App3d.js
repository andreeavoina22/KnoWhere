import { Scene, PerspectiveCamera, WebGLRenderer } from 'three';
import OrbitControls from 'orbit-controls-es6';
import Updater from './updater';

class App3d {
    constructor() {
        this.container = document.getElementById('webgl');
        this.scene = new Scene();
        this.camera = new PerspectiveCamera(75, this.container.offsetWidth/this.container.offsetHeight, 0.1, 1000);
        this.renderer = new WebGLRenderer();
        this.renderer.setSize( this.container.offsetWidth, this.container.offsetHeight );
        this.container.appendChild(this.renderer.domElement);
        this.controls = new OrbitControls(this.camera, this.container);
        this.updater = new Updater(this.scene, this.camera, this.renderer, this.controls);
    }

    start() {
        this.updater.start();
    }
}

export default App3d;