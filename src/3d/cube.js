import { Object3D, CubeGeometry, MeshBasicMaterial, Mesh } from 'three';

class Cube extends Object3D {
  constructor() {
    super();
    const geometry = new CubeGeometry( 100, 100, 100);
    const material = new MeshBasicMaterial({ color: 0x00ff00 });
    this.mesh = new Mesh(geometry, material);
    this.add(this.mesh);
  }
}

export default Cube;